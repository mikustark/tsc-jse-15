package ru.tsc.karbainova.tm.api.controller;

public interface IProjectController {
    void showProject();

    void clearProject();

    void createProject();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();
}
