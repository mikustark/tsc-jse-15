package ru.tsc.karbainova.tm.component;

import ru.tsc.karbainova.tm.api.controller.ICommandController;
import ru.tsc.karbainova.tm.api.controller.IProjectController;
import ru.tsc.karbainova.tm.api.controller.ITaskController;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.IProjectToTaskService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.controller.CommandController;
import ru.tsc.karbainova.tm.controller.ProjectController;
import ru.tsc.karbainova.tm.controller.TaskController;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.CommandRepository;
import ru.tsc.karbainova.tm.repository.ProjectRepository;
import ru.tsc.karbainova.tm.repository.TaskRepository;
import ru.tsc.karbainova.tm.service.CommandService;
import ru.tsc.karbainova.tm.service.ProjectService;
import ru.tsc.karbainova.tm.service.ProjectToTaskService;
import ru.tsc.karbainova.tm.service.TaskService;

import java.util.Scanner;

import static ru.tsc.karbainova.tm.constant.ArgumentConst.*;
import static ru.tsc.karbainova.tm.constant.ArgumentConst.VERSION;
import static ru.tsc.karbainova.tm.constant.TerminalConst.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectToTaskService projectToTaskService = new ProjectToTaskService(projectRepository, taskRepository);

    private final ITaskController taskController = new TaskController(taskService, projectService, projectToTaskService);
    private final IProjectController projectController = new ProjectController(projectService, projectToTaskService);

    public void start(String[] args) {
        commandController.displayWelcome();
        initDate();
        run(args);
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                parseCommand(command);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void initDate() {
        projectService.add(new Project("Pro C", "-"));
        projectService.add(new Project("Pro A", "-"));
        projectService.add(new Project("Pro B", "-"));
        taskService.add(new Task("Pro C", "-"));
        taskService.add(new Task("Pro A", "-"));
        taskService.add(new Task("Pro B", "-"));
    }

    public void parseArg(final String param) {
        switch (param) {
            case HELP:
                commandController.displayHelp();
                break;
            case ABOUT:
                commandController.displayAbout();
                break;
            case INFO:
                commandController.showInfo();
                break;
            case VERSION:
                commandController.displayVersion();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.displayErrorCommand();
        }
    }

    public void parseCommand(final String param) {
        switch (param) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_EXIT:
                commandController.exit();
                break;
            case CLEAR_PROJECT:
                projectController.clearProject();
                break;
            case CREATE_PROJECT:
                projectController.createProject();
                break;
            case LIST_PROJECT:
                projectController.showProject();
                break;
            case LIST_TASK:
                taskController.showTask();
                break;
            case CLEAR_TASK:
                taskController.clearTask();
                break;
            case CREATE_TASK:
                taskController.createTask();
                break;
            case UPDATE_BY_ID_PROJECT:
                projectController.updateById();
                break;
            case UPDATE_BY_INDEX_PROJECT:
                projectController.updateByIndex();
                break;
            case REMOVE_BY_ID_PROJECT:
                projectController.removeById();
                break;
            case REMOVE_BY_INDEX_PROJECT:
                projectController.removeByIndex();
                break;
            case REMOVE_BY_NAME_PROJECT:
                projectController.removeByName();
                break;
            case FIND_BY_ID_PROJECT:
                projectController.showById();
                break;
            case FIND_BY_INDEX_PROJECT:
                projectController.showByIndex();
                break;
            case FIND_BY_NAME_PROJECT:
                projectController.showByName();
                break;
            case UPDATE_BY_ID_TASK:
                taskController.updateById();
                break;
            case UPDATE_BY_INDEX_TASK:
                taskController.updateByIndex();
                break;
            case REMOVE_BY_ID_TASK:
                taskController.removeById();
                break;
            case REMOVE_BY_INDEX_TASK:
                taskController.removeByIndex();
                break;
            case REMOVE_BY_NAME_TASK:
                taskController.removeByName();
                break;
            case FIND_BY_ID_TASK:
                taskController.showById();
                break;
            case FIND_BY_INDEX_TASK:
                taskController.showByIndex();
                break;
            case FIND_BY_NAME_TASK:
                taskController.showByName();
                break;
            case STATUS_START_BY_ID_PROJECT:
                projectController.startById();
                break;
            case STATUS_START_BY_INDEX_PROJECT:
                projectController.startByIndex();
                break;
            case STATUS_START_BY_NAME_PROJECT:
                projectController.startByName();
                break;
            case STATUS_FINISH_BY_ID_PROJECT:
                projectController.finishById();
                break;
            case STATUS_FINISH_BY_INDEX_PROJECT:
                projectController.finishByIndex();
                break;
            case STATUS_FINISH_BY_NAME_PROJECT:
                projectController.finishByName();
                break;
            case STATUS_START_BY_ID_TASK:
                taskController.startById();
                break;
            case STATUS_START_BY_INDEX_TASK:
                taskController.startByIndex();
                break;
            case STATUS_START_BY_NAME_TASK:
                taskController.startByName();
                break;
            case STATUS_FINISH_BY_ID_TASK:
                taskController.finishById();
                break;
            case STATUS_FINISH_BY_INDEX_TASK:
                taskController.finishByIndex();
                break;
            case STATUS_FINISH_BY_NAME_TASK:
                taskController.finishByName();
                break;
            case TASK_UNBIND_BY_ID:
                taskController.taskUnbindById();
                break;
            case BIND_TASK_TO_PROJECT_BY_ID:
                taskController.bindTaskToProjectById();
                break;
            case FIND_ALL_TASK_BY_PROJECT_ID:
                taskController.findAllTaskByProjectId();
                break;
            default:
                throw new UnknowCommandException();
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        parseArg(param);
        System.exit(0);
    }
}
